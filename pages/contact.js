import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";

const contact = () => {
  return (
    <>
      <Head>
        <title>PBT India | Contact Us</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <div className="pagehead">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <h1>Contact Us</h1>
            </div>
          </div>
        </div>
      </div>

      <div className="page-wrapper">
        <div className="container">
          <div className="row">
            <div class="col-sm-8">
              <div class="row">
                <div className="col-sm-6 text-center">
                  <span className="expert-title mb-2">PBT Central Office</span>
                  <div className="d-flex align-items-center justify-content-center mb-2">
                    <div>
                      <Image
                        src="/location.svg"
                        alt="location-icon"
                        width={26}
                        height={26}
                      />
                    </div>
                    <div className="ms-2">
                      <p className="mb-0">
                        Commercial Point (Ground Floor) <br></br>79 Lenin
                        Sarani, Kolkata 700013
                      </p>
                    </div>
                  </div>
                  <div className="d-flex align-items-center justify-content-center mb-2">
                    <div>
                      <Image
                        src="/call.svg"
                        alt="call-icon"
                        width={23}
                        height={23}
                      />
                    </div>
                    <div className="ms-2">
                      <p className="mb-0">9836706952, 9831983670</p>
                    </div>
                  </div>
                  <div className="d-flex align-items-center justify-content-center">
                    <div>
                      <Image
                        src="/mail.svg"
                        alt="mail-icon"
                        width={26}
                        height={26}
                      />
                    </div>
                    <div className="ms-2">
                      <p className="mb-0">pbtindia2012@gmail.com</p>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6 text-center">
                  <span className="expert-title mb-2">Dr. Kunal Saha</span>
                  <p className="mt-0 mb-2">
                    President, PBT India <br></br>Adjunct Professor and HIV/AIDS
                    Consultant
                  </p>
                  <div className="d-flex align-items-center justify-content-center mb-2">
                    <div>
                      <Image
                        src="/location.svg"
                        alt="location-icon"
                        width={26}
                        height={26}
                      />
                    </div>
                    <div className="ms-2">
                      <p className="mb-0">Ohio, USA</p>
                    </div>
                  </div>
                  <div className="d-flex align-items-center justify-content-center mb-2">
                    <div>
                      <Image
                        src="/call.svg"
                        alt="call-icon"
                        width={23}
                        height={23}
                      />
                    </div>
                    <div className="ms-2">
                      <p className="mb-0">001-614-893-6772</p>
                    </div>
                  </div>
                  <div className="d-flex align-items-center justify-content-center">
                    <div>
                      <Image
                        src="/mail.svg"
                        alt="mail-icon"
                        width={26}
                        height={26}
                      />
                    </div>
                    <div className="ms-2">
                      <p className="mb-0"> anku1@earthlink.net</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default contact;
