import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.css';
import { Merriweather } from '@next/font/google';
import localFont from '@next/font/local';
import { useEffect } from "react";



const merriweather = Merriweather({
  weight:["400","700"],
  display:"swap",
  variable : "--font-merriweather"
})

const avenir = localFont({ 
  src: '../public/fonts/avenir.otf',
  display:"swap",
  variable:"--font-avenir"
 })

 const avenirBold = localFont({ 
  src: '../public/fonts/avenir-bold.otf',
  display:"swap",
  variable:"--font-avenirBold"
 })

 const avenirExtraBold = localFont({ 
  src: '../public/fonts/avenir-extrabold.otf',
  display:"swap",
  variable:"--font-avenirExtraBold"
 })

export default function App({ Component, pageProps }) {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);
  return (
    <div className={`${avenir.variable} ${avenirBold.variable} ${avenirExtraBold.variable} ${merriweather.variable}`}>
      <Component {...pageProps} />
    </div>
  )
}
