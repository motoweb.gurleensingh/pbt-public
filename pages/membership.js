import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import Head from 'next/head';
import Image from "next/image";
import Link from "next/link";

const membership = () => {
    return ( 
    <>
        <Head>
        <title>PBT India | Membership</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        </Head>
        <Navbar/>
        <div className="pagehead">
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <h1>Membership</h1>
                    </div>
                </div>
            </div>
        </div>
        
        <div className="page-wrapper">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8">
                        <p>Membership is not important to join the movement. Your support and voice against medical malpractice counts. Any person can become a member of PBT.  The organization would help any person to voice against medical malpractice, even if he/she is not a member. Although PBT’s founding president, Dr. Kunal Saha, bears most of PBT’s expenses and PBT has not accepted a single rupee from any government source till today, your decision to join PBT and help us win this uneven battle against a powerful and corrupt medical lobby will go a long way to fix India’s grossly flawed healthcare delivery system.</p>

                        <div className="sections">
                                <h1 className="post-title">Indian Citizens:</h1>
                                <ol>
                                    <li className="mb-2"><strong>Patrons:</strong> Rs. 10, 000 and above,  (one time subscription)</li>

                                    <li className="mb-2"><strong>Honorary:</strong> Not Applicable (only at the discretion of the President)</li>

                                    <li className="mb-2"><strong>Regular:</strong> Rs. 500 (per year)</li>

                                    <li className="mb-2"><strong>Life:</strong> Rs. 5000 and above, (one time subscription)</li>
                                </ol>
                                <div className="row align-items-center">
                                    <div className="col-sm-6 pe-3">
                                        <Link download href="/membership/pbt-membership-form-indian.doc" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/doc.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">PBT Membership Form (Indian)</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                        </div>

                        
                        

                        <div className="sections">
                                <h1 className="post-title">For citizens of countries other than India:</h1>
                                <ol>
                                    <li className="mb-2"><strong>Patrons:</strong> US $350.00 (one time subscription)</li>

                                    <li className="mb-2"><strong>Honorary:</strong> Not Applicable (only at the discretion of the President)</li>

                                    <li className="mb-2"><strong>Regular:</strong> US $50.00 (per year)</li>

                                    <li className="mb-2"><strong>Life Member:</strong> US $250.00 (one time subscription)</li>
                                </ol>
                                <div className="row align-items-center">
                                    <div className="col-sm-6 pe-3">
                                        <Link download href="/membership/pbt-membership-form-foreign.doc" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/doc.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">PBT Membership Form (Foreign)</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                        </div>

                       

                        <p>Send your filled up membership form along with payment in the form of A/C payee cheque or draft in favor of ‘People for better treatment’ to PBT office. Do not send any cash.</p>

                        <p><i>Note: The signatories to the Memorandum of the Association of the Society & the Office Bearers of the Governing Body shall be the first members of the Society. In general, only the Governing Body of PBT will have the power to admit members. The Governing Body may refuse to admit any person(s) as a member without showing any reason. The President of PBT will have a “veto” power to overrule any decision of the Governing body and only the President will have the power to assign “Honorary Membership” to any individual.</i></p>

                        
                    </div>
                </div>
            </div>
        </div>
        <Footer/>
        
    </> 
    );
}
 
export default membership;