import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';

const victims = () => {
    return ( 
        <>
            <Head>
            <title>PBT India | Financial Support</title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar/>
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>Financial Support</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 text-sm-start text-center">
                            <p>At this point of time, we may not be able to provide direct financial support to any victims of malpractice. However, in the future when our financial status would permit, we will try to help financially those victims of malpractice who otherwise may not be able to afford to find justice. Service for the society by any member of PBT will never be a “work for gain” and as such, any contribution (financial or otherwise) to the society will be exclusively “charitable”.</p>

                            <p>The total income properties of the Society through its members or any other sources shall be used solely towards the promotion of the objectives of the Society and no portion thereof shall be paid to or divided amongst any of its members by the way of profit. As a “not for profit” organization, we hope to obtain exemption for taxes for any contribution made to the Society. A detail annual budget of the Society will be published and made available as “Public Information”.</p>
                          
                            <p>Disclaimer : PBT doesn’t corroborate the medical expert opinions of the doctors mentioned above. This page is created with the intention of helping medical negligence victims get in touch with Doctors who are willing to provide expert testimony in pursuit of justice.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            
        </>
     );
}
 
export default victims;