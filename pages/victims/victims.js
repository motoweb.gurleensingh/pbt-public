import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';

const victims = () => {
    return ( 
        <>
            <Head>
            <title>PBT India | Victims</title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar/>
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>Victims</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8">
                            <p>In the event of an incidence of medical negligence, following are the steps and process you need to follow to maximize your chance of getting justice.</p>
                            <ol>
                                <li className="mb-3">In case of death, insist on post mortem.</li>
                                <li className="mb-3">Based on post mortem file a police complaint under Section 304 of IPC. Even if a post mortem has not been done, file a police report.</li>
                                <li className="mb-3">Make immediate written request for complete Medical Records citing Section 1.3.2 of MCI “Code of Ethics & Regulations”. All hospitals/nursing homes must honor the request WITHIN 72 HOURS. Please see format for requesting medical records (word).</li>
                                <li className="mb-3">Write a complaint with detail evidences (Strictly Photocopies) to the state medical council with copy to PBT, Medical Council of India (MCI) and also the State Health Ministry; Be prepared to record statements during cross examination and ask for a certified copy of statements recorded as soon as possible after a hearing.</li>
                                <li className="mb-3">After waiting 6 months, file a complaint to the MCI for transfer of your case (if the state medical council did not act).</li>
                                <li className="mb-3">If the State Government colludes, complain to the State Vigilance Officer/Lokayukta.</li>
                                <li className="mb-3">File a complaint with Joint Commission International (JCI) and National Accreditation Board for Hospitals and Healthcare providers (NABH) if the hospital is accredited with either of them.</li>
                                <li className="mb-3">Discuss for possible “civil” (for compensation) within 2 years and “criminal” case, where appropriate; Prepare the case taking the help of a doctor so that points of medical negligence are clearly spelled out and there is enough supporting documentation.</li>
                                <li className="mb-3">Never, ever handover any original document of treatment or any other evidence to anybody, not even to your lawyers, friends or any organization, not even to PBT. If possible submit only attested photocopies in the Courts after showing the originals to the Hon’ble Judges/ investigators.</li>
                            </ol>

                            <div className="sections">
                                <h1 className="post-title">What you need to be prepared for?</h1>
                                <p>Getting justice in medical negligence cases are an uphill task since the onus is on the complainant to prove medical negligence in a system known for inordinate delays and where the case will be decided by doctors and likely to rule in favour of doctors. However, as general awareness of society improves, finding justice is progressively getting better. We need a lot of individuals like yourselves to fight for justice so that a better system and society evolves for our kids.</p>
                            </div>
                            
                            <div className="sections">
                                <h1 className="post-title">Why you need to fight?</h1>
                                <p>There is some meaning to the life lost or injury sustained as a result of these atrocious acts of negligence. If these errant doctors aren’t stopped, they will go on to harm other individuals. That would be a dangerous society for our kids to live in. Finally, simple fairness of society requires that people who make mistakes that inflict untold suffering on other human beings must be punished appropriately.</p>
                            </div>

                            <div className="sections">
                                <h1 className="post-title">Need more Help?</h1>
                                <p>Contact one of the PBT volunteers.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            
        </>
     );
}
 
export default victims;