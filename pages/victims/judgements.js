import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';
import Image from "next/image";
import Link from "next/link";

const victims = () => {
    return ( 
        <>
            <Head>
            <title>PBT India | Judgements</title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar/>
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>Judgements</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="page-wrapper">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-8">
                            <div className="sections mt-0">
                                <h1 className="post-title mb-xxl-5 mb-4"> Supreme Court Judgements</h1>
                                <div className="row">
                                <div className="col-sm-6 pe-3">
                                    <Link href="/victims/judgements/SC-Judgment-Anu-case-compensation-oct-24-2013.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (Anu case compensation) oct 24, 2013</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-Feb-20101.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (Feb 2010)</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-2009-Katzu1.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (2009) Katzu</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-2008-Kholi1.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (2008) Kholi</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-Spring-Medows1.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">Judgment (Spring Medows)</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-Oct.-2010-Remanded-back-to-NCDRC.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (Oct. 2010) Remanded back to NCDRC</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-Lata-Wadha.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (Lata Wadha)</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-Savita-Garg.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (Savita Garg)</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-2010-V-Kishan-Rao-vs.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (2010) V. Kishan Rao vs.</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-July-2010-Women.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (July 2010) Women</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-JJ-Merchant-2001.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (JJ Merchant) 2001</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-IMA-vs-Shanta.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (IMA vs Shanta)</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>

                                <div className="col-sm-6">
                                    <Link href="/victims/judgements/SC-Judgment-Hattangadi.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">SC Judgment (Hattangadi)</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                                </div>
                            </div>

                            <div className="sections">
                                <h1 className="post-title mb-xxl-5 mb-4">Cases before National Consumer Disputes Redressal Commmison (NCDRC)  </h1>
                                <div className="row align-items-center">
                                    <div className="col-sm-6 pe-3">
                                        <Link href="/victims/judgements/NCDRC-J-2005-4-Oct.pdf" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Shivaprakash, Prasad, Prabhudev vs Wockhardt Hospital Limited,</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>

                                    <div className="col-sm-6 pe-3">
                                        <Link href="/victims/judgements/NCDRC-J-2007-10-Aug-Gupta.pdf" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Dr. Pillai vs S. Sharma, Rajamma, ..</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>

                                    <div className="col-sm-6 pe-3">
                                        <Link href="/victims/judgements/NCDRC-J-2007-16-Aug.pdf" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Lohakpure vs Smt. Suniti Devi Singhania Hospital</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>

                                    <div className="col-sm-6 pe-3">
                                        <Link href="/victims/judgements/NCDRC-J-2007-18-July.pdf" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Sarwat Ali Khan vs Prof.(Dr.) R. Gogi, Dr. (Mrs.) Arti Nangia, Aligarh Muslim University</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>

                                    <div className="col-sm-6 pe-3">
                                        <Link href="/victims/judgements/NCDRC-J-2008-Feb-25.pdf" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Bombay Hospital vs Sharifabai Ismail Syed, Rafique Syed, Dr. (Miss) Mehar Dadachanji, Dr. Keki E. Turial</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>

                                    <div className="col-sm-6 pe-3">
                                        <Link href="/victims/judgements/NCDRC-J-2010-12-Feb-Res-Ipsa-p-25.pdf" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Dr. Kamal Kishore vs. Dr. Trehan, Escorts Heart Institute</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>

                                    <div className="col-sm-6 pe-3">
                                        <Link href="/victims/judgements/NCDRC-J-2009-13-Aug.pdf" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Joginder, Jasprit , Balwinder Singh vs. Dr. Majumdar, Dr. Khurana</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>

                                    <div className="col-sm-6 pe-3">
                                        <Link href="/victims/judgements/NCDRC-J-2007-9-July.pdf" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Smt. Saroj Chandhoke vs. Sir Ganga Ram Hospital, Dr. Bhandari</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>

                                    <div className="col-sm-6 pe-3">
                                        <Link href="/victims/judgements/NCDRC-J-2010-Sep-Good-on-quantum-Motor-accident-negligence.pdf" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Surendra Kumar Tyagi vs Jagat Nursing Home & Hospital, Dr. S K Sharma</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>


                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            
        </>
     );
}
 
export default victims;