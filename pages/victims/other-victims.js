import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';
import Link from "next/link";

const victims = () => {
    return ( 
        <>
            <Head>
            <title>PBT India | Other Victims</title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar/>
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>Other Victims</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 text-sm-start text-center">
                            <p>Below are links to other medical negligence victims who are fighting their injustice. If you are a victim and you have a blog or a facebook page or a petition to link to, please email us at <Link className="custom-link" href="mailto:info.pbtindia@gmail.com">info.pbtindia@gmail.com</Link></p>

                            <p>Varun Tandon lost his mother at Fortis, New Delhi<br></br><Link className="custom-link" target="_blank" href="http://fortisreview.com">http://fortisreview.com</Link></p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            
        </>
     );
}
 
export default victims;