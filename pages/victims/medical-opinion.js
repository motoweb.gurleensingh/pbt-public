import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';
import Image from "next/image";

const victims = () => {
    return ( 
        <>
            <Head>
            <title>PBT India | Expert Medical Opinion</title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar/>
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>Expert Medical Opinion</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 text-sm-start text-center">
                            <p>One of the very difficult hurdles in getting justice in medical negligence cases is obtaining expert medical testimony. However that is changing. Thanks to a very few set of brave doctors.</p>

                            <p>We want to build a critical mass of such doctors that over time it is no longer offensive to speak the truth and explain what the standard of care should have been.</p>
                          
                            <p>If you are a doctor and willing to provide expert medical testimony to help hapless victims of medical negligence, please email us at info.pbtindia@gmail.com.</p>
                            

                            <div className="row">
                                <div className="col-sm-6 mt-sm-5 mt-4 text-center">
                                    <span className="expert-title mb-2">Dr. Ravikumar Bhaskaran</span>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/location.svg" alt="location-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">Trivandrum, Kerala</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/call.svg" alt="call-icon" width={23} height={23}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">93493-12325, 0471-2727659</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center">
                                        <div>
                                            <Image src="/mail.svg" alt="mail-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">bhaskaran.ravikumar@gmail.com</p>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-sm-6 mt-5 text-center">
                                    <span className="expert-title mb-2">Dr. Devadass P.K</span>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/location.svg" alt="location-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">Bangalore, Karnataka</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/call.svg" alt="call-icon" width={23} height={23}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">93437-67714, 76763-57797</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center">
                                        <div>
                                            <Image src="/mail.svg" alt="mail-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">devadasspk23@gmail.com</p>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-sm-6 mt-5 text-center">
                                    <span className="expert-title mb-2">Dr. Kunal Saha</span>
                                    <p className="mt-0 mb-2">Infectious Diseases (HIV/AIDS)</p>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/location.svg" alt="location-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">Ohio, USA</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/call.svg" alt="call-icon" width={23} height={23}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">001-614-893-6772</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center">
                                        <div>
                                            <Image src="/mail.svg" alt="mail-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">anku1@earthlink.net</p>
                                        </div>
                                    </div>
                                </div>


                                <div className="col-sm-6 mt-5 text-center">
                                    <span className="expert-title mb-2">Dr. Dipak Ranjan Das</span>
                                    <p className="mt-0 mb-2">Cardiologist & Critical Care Consultant</p>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/location.svg" alt="location-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">Cuttack, Orissa</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/call.svg" alt="call-icon" width={23} height={23}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">94371-65904</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center">
                                        <div>
                                            <Image src="/mail.svg" alt="mail-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">maildrdipak@hotmail.com</p>
                                        </div>
                                    </div>
                                </div>


                                <div className="col-sm-6 mt-5  text-center">
                                    <span className="expert-title mb-2">Dr. Samir Rai</span>
                                    <p className="mt-0 mb-2">Urologist</p>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/location.svg" alt="location-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">Amritsar, Punjab</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center mb-2">
                                        <div>
                                            <Image src="/call.svg" alt="call-icon" width={23} height={23}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">98141-52929</p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-center">
                                        <div>
                                            <Image src="/mail.svg" alt="mail-icon" width={26} height={26}/>
                                        </div>
                                        <div className="ms-2">
                                            <p className="mb-0">rai.hospital@gmail.com</p>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            
        </>
     );
}
 
export default victims;