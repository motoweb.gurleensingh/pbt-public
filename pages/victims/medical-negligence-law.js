import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';
import Image from "next/image";
import Link from "next/link";

const victims = () => {
    return ( 
        <>
            <Head>
            <title>PBT India | Medical negligence Law</title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar/>
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>Medical Negligence Law</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 text-sm-start text-center">
                            <p>Many victims are not familiar with the law, leave alone being familiar with the law as it pertains to medical negligence. They are ordinary individuals who want to get justice for the wrong that has been meted out to them.</p>

                            <p>Getting educated on medical negligence law is critical in effectively pursuing justice. The attached short document is an excellent introduction to the law and interpretation of the law of medical negligence. It starts with defining negligence and goes on to explain the standard with which professional negligence is assessed.</p>
                          
                            <p>It also covers the landmark judgments on the topic of medical negligence and how courts have progressively defined and clarified on this matter. It is an excellent introduction for anyone trying to pursue justice in the Indian Legal System.</p>
                            
                            <div className="row">
                                <div className="col-sm-12">
                                    <Link href="/victims/medical-negligence.pdf" className="files-div" target="_blank">
                                        <div className="d-flex align-items-center  mb-2">
                                            <div>
                                                <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                            </div>
                                            <div className="ms-2">
                                                <p className="mb-0">Medical Negligence - Law and Interpretation</p>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            
        </>
     );
}
 
export default victims;