import axios from "axios";
import HOST from "../../config/config"
import { useState, useEffect } from "react";
import { useRouter } from 'next/router'
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';
import Image from "next/image";
import Link from "next/link";
import {
    FacebookShareButton,
    FacebookIcon,
    TwitterShareButton,
    TwitterIcon,
} from 'next-share'

export default function Preview({ props }) {
    const [data, setData] = useState([])
    const [id, setId] = useState('')
    const { query } = useRouter();
    const router = useRouter();
    useEffect(() => {
        if (query.id) {
            setId(query.id);
        }
        // if (query.id) {
        //     console.log(query);
        //     axios.get(HOST + "/blogs/" + query.id).then(resp => {
        //         console.log(resp)
        //         setData(resp.data)
        //     })
        // }
    }, [router])

    return (
        <>
            <Head>
                <title>PBT India | {props?.data?.attributes?.title}</title>
                <meta name="description" content={props?.data?.attributes?.desc} />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar />
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>{props?.data?.attributes?.title}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="mb-4"></div>
                            <p className="color-gray">{props?.data?.attributes?.dated}</p>
                            <p>{props?.data?.attributes?.desc}</p>
                            {data?.data?.attributes?.press?.data ? <Link href={`preview?url=${props?.data?.attributes?.url}`}>Press Release</Link> : ""}

                            <FacebookShareButton
                                url={'https://pbtindia.com/preview/' + query.id}
                                quote={'PBT'}
                                hashtag={'#pbt'}
                            >
                                <FacebookIcon size={32} round />
                            </FacebookShareButton>

                            <TwitterShareButton
                                url={'https://pbtindia.com/preview/' + query.id}
                                quote={'PBT'}
                                hashtag={'#pbt'}
                            >
                                <TwitterIcon size={32} round />
                            </TwitterShareButton>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

Preview.getInitialProps = async (context) => {
    const res = await fetch(HOST + '/blogs/' + context.query.id);
    const json = await res.json()

    console.log("Hi there", json)
    return {
        props: json,
    };
};
