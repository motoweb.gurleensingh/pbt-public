import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import Head from 'next/head';

const supportUs = () => {
    return ( 
    <>
        <Head>
        <title>PBT India | Support Us</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        </Head>
        <Navbar/>
        <div className="pagehead">
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <h1>Support Us</h1>
                    </div>
                </div>
            </div>
        </div>
        
        <div className="page-wrapper">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8">
                        <div className="sections mt-0">
                            <h1 className="post-title">Support People for Better Treatment</h1>
                            <p>People for Better Treatment (PBT) has been fighting numerous legal and public battles including the recent battle against corruption in the Medical Council of India (MCI) to stop “medical negligence” and to improve the standard of medical education in India.</p>

                            <p>We urge all conscientious citizens to join us as volunteers and to generously donate to the PBT to help our endeavor in this enormous battle for humanity and to bring an end to the pervasive “medical negligence” and rampant corruption in Indian healthcare.</p>
                        </div>
                        <strong>You can make donation to PBT through any one of the followings:</strong>

                        <div className="accordion mt-3" id="accordionExample">
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingOne">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                Donate using your credit card or your “Paypal” account
                                </button>
                                </h2>
                                <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    <p>Donate Now</p>
                                </div>
                                </div>
                            </div>
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingTwo">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Direct deposit to the PBT account
                                </button>
                                </h2>
                                <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    <p>People for Better Treatment; <br></br>A/c No. 10559214004;  <br></br>State Bank of India, Dakshineswar Branch, 2/1 T. N. Biswas Road, Kolkata 700035</p>
                                </div>
                                </div>
                            </div>
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingThree">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Mail a check  in the name of
                                </button>
                                </h2>
                                <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    <p>“People for Better Treatment”, Commercial Point (Ground floor), 79 Lenin Sarani, Kolkata 700013.</p>
                                </div>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <Footer/>
        
    </>  
    );
}
 
export default supportUs;