import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from "next/head";
import Link from "next/link";

const anuradha6 = () => {
    return ( 
        <>
        <Head>
          <title>PBT India | Anuradha's Story</title>
          <meta name="description" content="" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Navbar />
        <div className="pagehead">
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <h1>Anuradha's Story</h1>
                <Link className="story-link" href="anuradha-story1">Part 1</Link>
                <Link className="story-link" href="anuradha-story2">Part 2</Link>
                <Link className="story-link" href="anuradha-story3">Part 3</Link>
                <Link className="story-link" href="anuradha-story4">Part 4</Link>
                <Link className="story-link" href="anuradha-story5">Part 5</Link>
                <Link className="story-link active" href="anuradha-story6">Part 6</Link>
              </div>
            </div>
          </div>
        </div>
  
        <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8">
                            <div className="sections mt-0">
                                <h1 className="post-title">Part 6. Final chapter: “Love story” in Paradise</h1>
                                <p>Today Kunal leads a normal and busy life in America since the tragic loss of his beloved wife in India. Along with his cutting edge medical research on HIV/AIDS, Kunal has been raging an enormous legal as well as social battle in India over the past 11 years to bring justice not only for his departed wife but also for the countless victims of “medical negligence” in India. The wheel of time has stopped for Kunal since the incomprehensible demise of Anuradha. But just as he felt on that ominous and dark night on 28th May, 1998 at the Breach Candy Hospital in Mumbai, there is a much bigger purpose for all this – even Anuradha’s death. After all, th\eir match was made in the heaven. Kunal feels her in every move that he takes and candidly confesses to his friends that all the strength that he needed to continue his AIDS research and fight this seemingly impossible battle against all odds in India actually comes from Her. It is just like after their marriage when Kunal and Anuradha had to be separated for sometime despite their passionate love and attraction for each other. Kunal feels his love for Anuradha is as intense today as it has ever been – it is an angelic love that never lessens. Anuradha is watching from the above and waiting for her day of justice just like millions of other innocent souls (victims of medical negligence) who are also waiting for their day of justice on this earth. During the hard struggle after their marriage, Anuradha promised that they would be living in the most beautiful house some day. But she never got a chance to return to their first dream house in Ohio. Anuradha is never wrong. She already has the most beautiful house ready for Kunal – it is the most relaxing and gorgeous house in paradise that we, people in this world can’t even fathom. But Kunal must wait until his crusade in this world is completed. Lying on the lush, green lawn in front of his house in Columbus on a clear, starry night, Kunal even tried to take a peek at their exquisite house in the paradise. He knows that Anuradha is waiting. He can hardly wait any longer.</p>

                                <h1 className="post-title">Acknowledgement</h1>
                                
                                    <div className="row">
                                        <div className="col-6">
                                            <ul>
                                                <li>Late Prof. Salil Bhattacharya (India)</li>
                                                <li>Prof. J.C. Roujeau (France)</li>
                                                <li>Prof. David Fine (USA)</li>
                                                <li>Prof. Peter Fritsch (Austria)</li>
                                                <li>Prof. Philip Walson (USA)</li>
                                                <li>Prof. G. Pierard (Belgium)</li>
                                                <li>Dr. J.S. Pasricha (India)</li>
                                                <li>Dr. Manuel Caruso (Canada)</li>
                                            </ul>
                                        </div>
                                        <div className="col-6">
                                            <ul>
                                                <li>Dr. Sukamal Saha (USA)</li>
                                                <li>Dr. A.K. Gupta (India)</li>
                                                <li>Dr. D.P. Mallick (India)</li>
                                                <li>Mr. Debdut Ghosh-Thakur (India)</li>
                                                <li>Mr. M.K. Ganguly (India)</li>
                                                <li>Mr. M. Banerjee (India)</li>
                                                <li>Mr. R. Venkataraman (India)</li>
                                                <li>Mr. M.N. Krishnamani (India)</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>and thousands of innocent victims of “medical negligence” and ordinary people from India and other countries.</p>

                                    <h2 className="sub-post-title mb-5">Based on a true story by Dr. Kunal Saha, Ohio, USA</h2>

                                <strong>NOTE: Any copying or public presentation without the written consent of the author is strictly prohibited</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <Footer />
    </> 
     );
}
 
export default anuradha6;