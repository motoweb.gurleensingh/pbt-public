import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';

const ourMission = () => {
    return ( 
        <>
            <Head>
            <title>PBT India | Our Misson</title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar/>
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>Our Mission</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8">
                            <p>The main aim of PBT India is to eradicate medical negligence and promote corruption-free healthcare in India. We intend to accomplish this goal by working on 3 broad areas:</p>

                            <div className="sections">
                                <h1 className="post-title">1. Help Victims to fight for Justice</h1>
                                <p>We provide them with information on the various avenues they have got in finding justice and the procedures for doing the same. We put victims in touch with other victims who have gone down a similar path so they can learn from past experience. We introduce them to medical and legal experts who can help them in their battle. We also lend support to their cases in appropriate forums so that we maximize the chance of justice getting done. Please see our victims sections for more.</p>
                            </div>

                            <div className="sections">
                                <h1 className="post-title">2. Public Watch dog</h1>
                                <p>There are several government and quasi-government entities that are entrusted with the responsibility of ensuring quality of healthcare system in India. These include the Union and State Ministries of Health, the Medical Council of India and the various state medical councils. We act as a public watch dog in ensure that these institutions are functioning effectively in preventing medical negligence. Please see our pending RTI applications & PIL actions for more.</p>
                            </div>

                            <div className="sections">
                                <h1 className="post-title">3. Improve Legislative, Regulatory & Legal Framework</h1>
                                <p>Our laws pertaining to healthcare and the effectiveness with which those are enforced need a lot of work. We actively work with the various legislative and regulatory bodies to ensure that effective laws and regulations are put in place. We work through these bodies and through the judicial system to ensure effective implementation of them. Please see our pending RTI applications & PIL actions for more.</p>

                                <p>Most of us have lost our loved ones to very careless and negligent actions of doctors. Our world gets turned upside down when we were least expecting it. The doctor and hospital that inflicted this enormous pain on us go completely unpunished. That is when the injustice of it all hits you in the head. There are three key reasons why this is an monumental social issue: it is prevalent; it has devastating implications; justice is almost impossible.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            
        </>
     );
}
 
export default ourMission;