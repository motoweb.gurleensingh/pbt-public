import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';

const medicalNegligence = () => {
    return ( 
        <>
            <Head>
            <title>PBT India | Why Medical Negligence</title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar/>
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>Why Medical Negligence</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 text-sm-start text-center">
                            <div className="sections">
                                <h1 className="post-title">1. It’s prevalent:</h1>
                                <p>Until you become a victim, most people don’t notice how prevalent this issue is. WHO estimates that Medical Errors is top 10% of killers in the world. With the number of victims coming to PBT India for help and the almost daily news items on incidence of medical negligence, it would be fair to say that thousands of lives are lost every month due to medical negligence. Those are not just numbers. Those are lost fathers, mothers, husbands, wives, daughters, sons and relatives.</p>
                            </div>

                            <div className="sections">
                                <h1 className="post-title">2. It has devastating implications:</h1>
                                <p>When Medical Negligence happens, our world turns upside down. It is not something that we can fix with time or money. In most cases, this happens when we are least expecting to lose our loved ones. And the pain is ongoing. Wives who have lost the companionship of their husband. Parents who miss the affectionate embrace of their kids. Kids growing up without the guidance of a dad or the affection of a mom. The shock and the pain can’t be expressed in words.</p>
                            </div>

                            <div className="sections">
                                <h1 className="post-title">3. Justice is almost impossible:</h1>
                                <p>Fairness requires that people who acted in a negligent manner and caused enormous pain to others should be punished appropriately. In India today, it is almost impossible to find a doctor negligent. Doctor’s don’t testify against other doctors. Benefit of doubt is given to the doctor and not to the victim. Even if a doctor is found guilty, the punishment is nebulous. In India, we rarely see doctors losing their license for negligent actions. The injustice of it all is unfathomable.</p>
                                <p>Most people realize it after becoming a victim. But, we want everyone to realize it beforehand. The possibility of losing our sons and daughter to a very careless and negligent doctor is real and high. Is that a world we want our sons and daughters to live in?</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            
        </>
     );
}
 
export default medicalNegligence;