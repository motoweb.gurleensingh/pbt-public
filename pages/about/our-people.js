import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";

const ourPeople = () => {
  return (
    <>
      <Head>
        <title>PBT India | Our People</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <div className="pagehead">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <h1>Our People</h1>
            </div>
          </div>
        </div>
      </div>

      <div className="page-wrapper">
        <div className="container">
          <div className="row">
            <div className="col-lg-8">
              <p>
                PBT India is run by very passionate and committed people with
                the mission to eradicate medical negligence and promote
                corruption-free healthcare in India. If you would like to join
                us in this important cause, please email us at{" "}
                <Link
                  className="custom-link"
                  href="mailto:pbtindia2012@gmail.com"
                >
                  pbtindia2012@gmail.com
                </Link>
              </p>

              <div className="col-sm-12">
                <div className="d-flex flex-sm-row flex-column">
                  <div className="flex-shrink-0">
                    <Image
                      src="/kunal-saha.png"
                      alt="Kunal Saha"
                      width={260}
                      height={260}
                    />
                  </div>
                  <div className="flex-grow-1 ms-sm-3">
                    <span className="expert-title mb-2 mt-sm-0 mt-3">
                      Dr. Kunal Saha
                    </span>
                    <p className="mt-0 mb-2">
                      President, PBT India <br></br>Adjunct Professor and
                      HIV/AIDS Consultant
                    </p>
                    <div className="d-flex align-items-center mb-2">
                      <div>
                        <Image
                          src="/location.svg"
                          alt="location-icon"
                          width={26}
                          height={26}
                        />
                      </div>
                      <div className="ms-2">
                        <p className="mb-0">Ohio, USA</p>
                      </div>
                    </div>
                    <div className="d-flex align-items-center mb-2">
                      <div>
                        <Image
                          src="/call.svg"
                          alt="call-icon"
                          width={23}
                          height={23}
                        />
                      </div>
                      <div className="ms-2">
                        <p className="mb-0">001-614-893-6772</p>
                      </div>
                    </div>
                    <div className="d-flex align-items-center ">
                      <div>
                        <Image
                          src="/mail.svg"
                          alt="mail-icon"
                          width={26}
                          height={26}
                        />
                      </div>
                      <div className="ms-2">
                        <p className="mb-0"> anku1@earthlink.net</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="sections">
                <h1 className="post-title">PBT Central Office:</h1>
                <p className="mb-1">
                  Mailing address: Commercial Point (Ground Floor) 79 Lenin
                  Sarani Kolkata 700013
                </p>
                <div className="d-flex">
                  <div>
                    <Image
                      src="/mail.svg"
                      alt="mail-icon"
                      width={26}
                      height={26}
                    />
                  </div>
                  <div className="ms-2">
                    <p className="mb-0">pbtindia2012@gmail.com</p>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">Mr. Mihir Banerjee </h6>
                        <p className="mt-0 mb-2">Vice-president</p>

                        <div className="d-flex align-items-center mb-1">
                        <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                        <div className="ms-2"><p className="mb-0">9831983670</p></div>
                        </div>

                        <div className="d-flex align-items-center">
                        <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                        </div>
                        <div className="ms-2">
                            <p className="mb-0">mihirbanerjee55@gmail.com</p>
                        </div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">Ms. Ratna Ghosh</h6>
                        <p className="mt-0 mb-2">Secretary</p>

                        <div className="d-flex align-items-center mb-1">
                        <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                        <div className="ms-2"><p className="mb-0">9836706952</p></div>
                        </div>

                        <div className="d-flex align-items-center">
                        <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                        </div>
                        <div className="ms-2">
                            <p className="mb-0">ratnaghosh57@yahoo.co.in</p>
                        </div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">Mrs. Jayeeta Verma-Sarkar</h6>
                        <p className="mt-0 mb-2">Member, Executive Committee</p>

                        <div className="d-flex align-items-center mb-1">
                        <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                        <div className="ms-2"><p className="mb-0">9830370743</p></div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">Mr. Saileswar Chakraborty</h6>
                        <p className="mt-0 mb-2">Member, Executive Committee</p>

                        <div className="d-flex align-items-center mb-1">
                        <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                        <div className="ms-2"><p className="mb-0">8777258818</p></div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">Mr. Ranjit Sarkar</h6>
                        <p className="mt-0 mb-2">Member, Executive Committee</p>

                        <div className="d-flex align-items-center mb-1">
                        <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                        <div className="ms-2"><p className="mb-0">6290591729</p></div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">Dr. Sarbajit Basu (Jt. Treasurer)</h6>
                        <p className="mt-0 mb-2">Member, Executive Committee</p>

                        <div className="d-flex align-items-center mb-1">
                        <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                        <div className="ms-2"><p className="mb-0">9674129556</p></div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">Mr. Syamal Bose (Jt. Treasurer)</h6>
                        <p className="mt-0 mb-2">Member, Executive Committee</p>

                        <div className="d-flex align-items-center mb-1">
                        <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                        <div className="ms-2"><p className="mb-0">9830303840</p></div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">Dr. Tripti Das</h6>
                        <p className="mt-0 mb-2">Member, Executive Committee</p>

                        <div className="d-flex align-items-center mb-1">
                        <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                        <div className="ms-2"><p className="mb-0">9874778832</p></div>
                        </div>
                    </div>
                  </div>

                </div>
                
              </div>

              <div className="sections">
                <h1 className="post-title">PBT Co-ordinators in major Indian cities:</h1>

                <div className="row">
                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">DELHI</h6>
                        <p className="mt-0 mb-2">Mr. Shishir Chand</p>

                        <div className="d-flex align-items-center mb-1">
                          <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                          <div className="ms-2"><p className="mb-0">98109-19282</p></div>
                        </div>

                        <div className="d-flex align-items-center">
                          <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                        </div>
                        <div className="ms-2">
                            <p className="mb-0">shishirchand@hotmail.com</p>
                        </div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">DELHI</h6>
                        <p className="mt-0 mb-2">Mr. Suraj Manchanda</p>

                        <div className="d-flex align-items-center mb-1">
                          <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                          <div className="ms-2"><p className="mb-0"> 98919-27669</p></div>
                        </div>

                        <div className="d-flex align-items-center">
                          <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                        </div>
                        <div className="ms-2">
                            <p className="mb-0">suraj_1511@yahoo.com</p>
                        </div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">BANGALORE</h6>
                        <p className="mt-0 mb-2">Ms. Roshni Pereira</p>

                        <div className="d-flex align-items-center mb-1">
                        <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                        <div className="ms-2"><p className="mb-0">91646-03034</p></div>
                        </div>

                        <div className="d-flex align-items-center">
                          <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                        </div>
                        <div className="ms-2">
                            <p className="mb-0">roshniper@gmail.com</p>
                        </div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">LUCKNOW</h6>
                        <p className="mt-0 mb-2">Mr. Sudhir Kumar Srivastava</p>

                        <div className="d-flex align-items-center mb-1">
                          <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                          <div className="ms-2"><p className="mb-0">94153-39652, 99172-81225</p></div>
                        </div>
                        <div className="d-flex align-items-center">
                          <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                          </div>
                          <div className="ms-2">
                              <p className="mb-0">srivastavasudhirkr@gmail.com</p>
                          </div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">AHMEDABAD</h6>
                        <p className="mt-0 mb-2">Mr. Ravish Bhatt</p>

                        <div className="d-flex align-items-center mb-1">
                          <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                          <div className="ms-2"><p className="mb-0">998985-50411</p></div>
                        </div>
                        <div className="d-flex align-items-center">
                          <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                          </div>
                          <div className="ms-2">
                              <p className="mb-0">ravishdbhatt@yahoo.com</p>
                          </div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">HOUSTON, USA</h6>
                        <p className="mt-0 mb-2">Mr. Vivek Kishore (Website Management)</p>

                        <div className="d-flex align-items-center mb-1">
                          <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                          <div className="ms-2"><p className="mb-0">+1-414-243-1047</p></div>
                        </div>
                        <div className="d-flex align-items-center">
                          <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                          </div>
                          <div className="ms-2">
                              <p className="mb-0">vivek_kishore@yahoo.com</p>
                          </div>
                        </div>
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="our-people">
                        <h6 className="title mb-1">AUSTIN, USA</h6>
                        <p className="mt-0 mb-2">Mr. Jey(PBT Co-Ordinator)</p>

                        <div className="d-flex align-items-center mb-1">
                          <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                          <div className="ms-2"><p className="mb-0">+1-512-426-9566</p></div>
                        </div>
                        <div className="d-flex align-items-center">
                          <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                          </div>
                          <div className="ms-2">
                              <p className="mb-0">ijeyaseelan@gmail.com</p>
                          </div>
                        </div>
                    </div>
                  </div>

                  <div className="col-auto">
                    <div className="our-people">
                        <h6 className="title mb-1">KERALA</h6>
                        <p className="mt-0 mb-2">Dr. Ravikumar Bhaskaran</p>

                        <div className="d-flex align-items-center mb-1">
                          <div><Image src="/call.svg" alt="call-icon" width={19} height={19}/></div>
                          <div className="ms-2"><p className="mb-0">93493-12325, 94961-53097</p></div>
                        </div>
                        <div className="d-flex align-items-center">
                          <div><Image src="/mail.svg" alt="mail-icon" width={20} height={20}/>
                          </div>
                          <div className="ms-2">
                              <p className="mb-0">ravikumar.bhaskaran1930@gmail.com</p>
                          </div>
                        </div>
                    </div>
                  </div>

                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default ourPeople;
