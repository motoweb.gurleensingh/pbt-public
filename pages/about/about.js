import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from 'next/head';
import Link from "next/link";
import Image from "next/image";

const about = () => {
    return ( 
        <>
            <Head>
            <title>PBT India | About Us</title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar/>
            <div className="pagehead">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>About Us</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8">
                            <p>PBT India was inaugurated on 30th December, 2001 at the Netaji Subash Institute, Sealdah by the founding President Dr. Kunal Saha from USA. What started as a fight for justice by an individual (Dr. Saha) against medical malpractice for the wrongful death of his wife Anuradha, has now turned into a mass movement against the pervasive malpractice across the country.</p>

                            <p>Anuradha, an India-born USA citizen died in the most incomprehensible manner during a social visit to India at an age of only 36 and at the beginning of a productive career as a Child Psychologist. She graduated from the prestigious Columbia University in New York City in 1998 just before her last trip to India. <Link className="custom-link" target="_blank" href="#">Please read Kunal and Anuradha story.</Link></p>

                            <p>In a historic judgment on “medical negligence” delivered on August 7, 2009, the Supreme Court of India found 4 doctors and Advanced Medicare Research Institute (AMRI) in Kolkata guilty for negligent treatment causing death of Anuradha. The Apex Court held senior medicine specialists Dr. Sukumar Mukherjee, Dr. Abani Roychowdhury, senior dermatologist Dr. Baidyanath Halder, physician Dr. Balaram Prasad (associated with the AMRI hospital) as well as the AMRI hospital, responsible for Anuradha’s death. In this SC Judgment, the SC has also laid down many new guidelines and rules that doctors/hospitals must observe for treatment of patients.</p>
                            <Link href="#" className="files-div" target="_blank">
                                            <div className="d-flex align-items-center  mb-2">
                                                <div>
                                                    <Image src="/pdf.svg" alt="pdf-icon" width={42} height={42}/>
                                                </div>
                                                <div className="ms-2">
                                                    <p className="mb-0">Please read details about this historic judgement</p>
                                                </div>
                                            </div>
                                        </Link>

                            <p>This historical judgment by the Supreme Court of India is a win for PBT and for all Indian citizens who until now stood silently with no hope for justice against the delinquent doctors of India who stayed above the law and continued the practice of wrongful medicine without any fear. We hope that this judgment will stimulate sweeping changes to bring honesty and transparency in the medical community and specially in the medical councils who have the noble duty to judge impartially the complaints against doctors and to stop malpractice in India. Anuradha Saha’s case has created momentum among the victims of medical negligence to join together to eradicate it from India. Today, PBT’s focus is not only to help the victims of medical negligence find justice but also to expose and fight the wide-spread medical corruption that has plagued the entire healthcare system and medical education putting grave danger for all of us and our future generations.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            
        </>
     );
}
 
export default about;