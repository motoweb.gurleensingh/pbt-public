import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from "next/head";
import Link from "next/link";

const anuradha2 = () => {
    return (  
    <>
        <Head>
          <title>PBT India | Anuradha's Story</title>
          <meta name="description" content="" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Navbar />
        <div className="pagehead">
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <h1>Anuradha's Story</h1>
                <Link className="story-link" href="anuradha-story1">Part 1</Link>
                <Link className="story-link active" href="anuradha-story2">Part 2</Link>
                <Link className="story-link" href="anuradha-story3">Part 3</Link>
                <Link className="story-link" href="anuradha-story4">Part 4</Link>
                <Link className="story-link" href="anuradha-story5">Part 5</Link>
                <Link className="story-link" href="anuradha-story6">Part 6</Link>
              </div>
            </div>
          </div>
        </div>
  
        <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8">
                            <div className="sections mt-0">
                                <h1 className="post-title">PART 2: The Gross “Medical Negligence”</h1>
                                <p>Anuradha eventually died at the Breach Candy Hospital in Mumbai after she was shifted there at the very last moment in a moribund condition from the AMRI Hospital in Calcutta. What started as a simple skin rash from allergic reaction to some drugs (allergic reaction can occur to almost any drug including simple antibiotics, pain relievers and even vitamin supplements), ended up taking Anuradha’s young life and shattered the fairy-tale love story between Anuradha and Kunal. Anuradha was first treated by Dr. Sukumar Mukherjee, called as “modern day Bidhan Roy and known to be the best in the city. Dr. Mukherjee advised a steroid (“Depomedrol”) in a manner which is unprecedented in medical parlance of the world. “Depomedrol” is a long-acting steroid with its action in our body lasts for a very long period. Unlike most regular steroids that are “short-acting” or “quick-acting) and removed from our body between 6-24 hours, “Depomedrol” can stay in our body for 14-28 days. This is why while other most regular steroid preparations can be given on a daily or even twice a day dosage in appropriate clinical conditions, “Depomedrol” can be given at a maximum dose of 40-120 mg at 1-2 weeks intervals (see below manufacturer’s “package insert” as well as numerous experts’ opinions). Furthermore, “Depomedrol” may be used only in some “chronic” conditions like asthma or arthritis because of its prolonged pharmacologic action.</p>

                                <p>Dr. Mukherjee prescribed “Depemedrol” for Anuradha when she developed skin rashes due to “acute” drug allergy. But most shockingly, Dr. Mukherjee advised “Depomedrol” for Anuradha at an astronomical dose of “80 mg twice daily”, i.e. at 20-50 times the maximum recommended dose of the drug. Even more reprehensible was the fact that Dr. Mukherjee was never able to diagnose the condition of Anuradha (“Toxic Epidermal Necrolysis” or TEN) but nevertheless, he prescribed “Depomedrol” at this massive overdose not once, but on two separate occasions – first on 7th May, 1998 at his chamber when Anuradha was very sick and then again on 11th May, 1998 at the AMRI hospital when the patient was in a far worse condition. The enormous amount of a “long-acting” steroid given by Dr. Mukherjee virtually wiped out Anuradha’s natural immunity (main side-effect of all steroids is “immunosuppression”) as she eventually died from “sepsis”.</p>
                                
                                <p>The two other primary accused doctors, Dr. Halder and Dr. Roychowdhury, came to see Anuradha after Dr. Mukherjee left the scene to attend a conference in USA on 11th May, 1998. Although these two doctors were able to correctly diagnose Anuradha’s condition as “TEN” and stopped “Depomedrol”, they also prescribed her even more steroids (“quick-acting”). More importantly, they did not follow any of the essential universally accepted treatment protocols for TEN which include immediate IV fluids and nutrients infusion, regular dressing of the skin wounds and test of blood to control infection. Despite an extremely debilitating condition with her mouth cavity full with raw sores from TEN, these doctors insisted to feed her hard “biscuits” with excruciating pain without caring to give nutrition through IV drip which is obligatory for proper treatment of TEN patients. They did not even advise to take care of Anuradha’s skin wound (because of loss of superficial layer of the skin) and kept her on the same filthy bed soaked with oozing body fluids. Under this absolutely abysmal condition and virtually without any treatment at the AMRI hospital, Anuradha quickly became infected and moribund as a result of this savage therapy. When Anuradha was almost under such unprecedented treatment at the AMRI hospital, these two doctors issued a certificate on 16th May, 1998 stating that she could be taken to the Breach Candy Hospital for better therapy. In a desperate attempt to save the life of his dearest wife, Kunal had to rent a chartered medical flight (because she was not in a condition to be taken in a regular commercial flight) and shifted Anuradha to Mumbai on 17th May, 1998. Although IV fluids, dressing and other essential protocols for treatment of TEN patients were immediately started at the Breach Candy Hospital, it was too little and too late. Anuradha eventually died at the Breach Candy Hospital on May 28, 1998 from “sepsis”. An innocent life is lost for no apparent reason. A loving and dynamic personality that touched the hearts of countless abused children and dedicated her life for the sake of the abused children was snatched away forever. The American Dream of a young couple who fought so hard against all odds was crushed just when they stepped one foot through the door of their dream house. Kunal was distraught, dazed and almost in a stance when Anuradha actually expired at the Breach Candy Hospital on the dreadful evening of 28th May, 1998. But something mystical and celestial happened at that moment which transformed the perspective of life for Kunal forever. The rest of this heartrending story is still being drafted in the courtrooms and hospitals across India with profound effects on the hearts and minds of countless victims of “medical negligence” and ordinary people in India. An article published in the Rediff News described the very last moment of Anuradha’s life in this earthly world (see in the next page).</p>

              
                                <Link href="anuradha-story3" className="next-link d-block mb-3">Next:Part 3</Link>

                                <strong>NOTE: Any copying or public presentation without the written consent of the author is strictly prohibited</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <Footer />
    </> 
     );
}
 
export default anuradha2;