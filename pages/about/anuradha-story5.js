import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";

const anuradha5 = () => {
    return ( 
        <>
        <Head>
          <title>PBT India | Anuradha's Story</title>
          <meta name="description" content="" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Navbar />
        <div className="pagehead">
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <h1>Anuradha's Story</h1>
                <Link className="story-link" href="anuradha-story1">Part 1</Link>
                <Link className="story-link" href="anuradha-story2">Part 2</Link>
                <Link className="story-link" href="anuradha-story3">Part 3</Link>
                <Link className="story-link" href="anuradha-story4">Part 4</Link>
                <Link className="story-link active" href="anuradha-story5">Part 5</Link>
                <Link className="story-link" href="anuradha-story6">Part 6</Link>
              </div>
            </div>
          </div>
        </div>
  
        <div className="page-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8">
                            <div className="sections mt-0">
                                <h1 className="post-title">PART 5: Medical Negligence” of Another Kind: Use of Defective HIV kits</h1>
                                <p>Reports of innocent patients becoming infected with HIV or Hepatitis viruses after receiving blood transfusion appear in the news on a regular basis in India. While this might not be considered as a rash and negligent act on part of a treating physician, it is the worst travesty for the defenseless patients in India. Nobody living in India is safe from the wrath of such atrocity in the practice of medicine. The primary reason for transfusion of virus-contaminated blood is the use of substandard diagnostic kits in the blood banks. The national AIDS control organization (NACO) has the sole authority to provide test kits for detection of deadly viruses like HIV, Dengue and Hepatitis to all blood banks and hospitals in India. Even the blood banks in private nursing homes and hospitals in India must obtain the test kits from NACO. PBT president Dr. Kunal Saha is an internationally renowned HIV/AIDS specialist based in USA. The World Bank hired Dr. Saha in 2007 (as a medical consultant) in their team to investigate allegations of defective HIV kits in India. It was uncovered during the investigation that despite serious complaints against malfunctioning HIV kits from major hospitals, NACO decided to keep silent and continue using the same test kits. (see below). The World Bank published a comprehensive report in 2008 showing rampant fraud and corruption with the AIDS control program during the second national AIDS control project (NACP-II) between 1999 and 2006.</p>

                                <p>Dr. Saha took a central role in bringing this serious situation to the attention of the Indian government and for knowledge of the ordinary people in India not only as his ethical duty as a registered physician but also for moral obligation for the sake of the hapless patients in India. The HIV kit scandal has been highlighted in the media. Even <em>Washington Post</em>, the prestigious national daily from USA, published three separate stories on this subject last year. But there has been no action against the corrupt individuals involved with this unimaginable human travesty. On the contrary, in order to suppress this incriminating episode of spurious HIV kits and to discredit Dr. Saha, NACO and other unscrupulous healthcare leaders in India started scathing personal attacks against Dr. Saha with utter lies and innuendoes. Even the World Bank showed little interest to go after the corrupt leaders at NACO or the dishonest manufacturers of spurious test kits. Nevertheless, Dr. Saha has raged a valiant fight to expose the unthinkable scam with HIV kits. In a PIL filed recently against NACO before the Delhi High Court (Writ Petition Civil No. 5973/2008), Dr. Saha has demanded exemplary punishment for the corrupt NACO members and compensation for the victims (living or dead) who contracted HIV as a result of use of spurious HIV kits in India. The National Human Rights Commission (NHRC) in Delhi has also initiated a <em>suo mota</em> case against the defective HIV kits (see below). Until this unthinkable travesty of supply of substandard diagnostic kits by government organizations like NACO can be totally eradicated by genuine investigation and exemplary punishment of the guilty individuals, every Indian citizen faces the danger of falling victim to deadly diseases like HIV and Hepatitis at any moment.</p>

                                <figure className="figure d-block">
                                    <Image src="/aboutus/part5/image1.webp" alt="anuradha's story" width={600} height={700} className="mb-2"/>
                                </figure>

                                <figure className="figure d-block">
                                    <Image src="/aboutus/part5/image2.webp" alt="anuradha's story" width={600} height={700} className="mb-2"/>
                                </figure>

                                <figure className="figure d-block">
                                    <Image src="/aboutus/part5/image3.webp" alt="anuradha's story" width={600} height={400} className="mb-2"/>
                                </figure>

              
                                <Link href="anuradha-story6" className="next-link d-block mb-3">Next:Part 6</Link>

                                <strong>NOTE: Any copying or public presentation without the written consent of the author is strictly prohibited</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <Footer />
    </> 
     );
}
 
export default anuradha5;