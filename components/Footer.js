import styles from "../styles/Footer.module.css"
import Link from "next/link";
import Image from "next/image";

const Footer = () => {
    return ( 
        <div className="container">
            <footer className = {styles.customfooter}>
                <div className="row">
                    <div className="col-sm-4">
                        <Link href="/"><Image src="/logo.jpg" alt="pbt-logo" width={200} height={55}/></Link>
                        <p className="color-gray mt-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.boris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in.</p>
                        <Link href="" className="me-3"><Image src="/fb.svg" alt="facebook-icon" width={30} height={30}/></Link>
                        <Link href=""><Image src="/ig.svg" alt="instagram-icon" width={29} height={29}/></Link>
                    </div>
                    <div className="col-sm-2 col-6 mt-sm-0 mt-4">
                        <h3>Quick Links</h3>
                        <ul>
                            <li><Link href="/">Home</Link></li>
                            <li><Link href="/membership">Membership</Link></li>
                            <li><Link href="/support-us">Support Us</Link></li>
                            <li><Link href="/contact">Contact Us</Link></li>
                        </ul>
                    </div>
                    <div className="col-sm-3 col-6 mt-sm-0 mt-4">
                        <h3>About Us</h3>
                        <ul>
                            <li><Link href="/about/about">Read About Us</Link></li>
                            <li><Link href="/about/our-mission">Our Mission</Link></li>
                            <li><Link href="/about/our-people">Our People</Link></li>
                            <li><Link href="/about/anuradha-story1">Anuradha’s Story</Link></li>
                            <li><Link href="/about/medical-negligence">Why Medical Negligence?</Link></li>
                        </ul>
                    </div>
                    <div className="col-sm-3 mt-sm-0 mt-4">
                        <h3>Victims</h3>
                        <ul>
                            <li><Link href="/victims/victims">Victims</Link></li>
                            <li><Link href="/victims/medical-opinion">Expert Medical Opinion</Link></li>
                            <li><Link href="/victims/financial-support">Financial Support</Link></li>
                            <li><Link href="/victims/medical-negligence-law">Medical Negligence Law</Link></li>
                            <li><Link href="/victims/judgements">Judgements </Link></li>
                            <li><Link href="/victims/other-victims">Other Victims </Link></li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
     );
}
 
export default Footer;