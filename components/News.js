import Link from "next/link";
import {
    FacebookShareButton,
    FacebookIcon,
    TwitterShareButton,
    TwitterIcon,
} from 'next-share'

export default function News(props) {
    return (
        <>
            <div className="mb-4"></div>
            <Link className="link" href={"/preview/" + props.id}><h2 className="post-heading">{props.title}</h2></Link>
            <p className="color-gray">{props.dated}</p>
            <p>{props.description}</p>
            {props.press.data ? <Link href={`preview?url=${props.press.data.attributes.url}`}>Press Release</Link> : ""}
            <br /><br /> <br />
            <FacebookShareButton
                url={'https://pbtindia.com/preview/' + props.id}
                quote={'PBT'}
                hashtag={'#pbt'}
            >
                <FacebookIcon size={32} round />
            </FacebookShareButton>

            <TwitterShareButton
                url={'https://pbtindia.com/preview/' + props.id}
                quote={'PBT'}
                hashtag={'#pbt'}
            >
                <TwitterIcon size={32} round />
            </TwitterShareButton>
        </>
    )
}
