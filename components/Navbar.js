import Link from "next/link";
import styles from "../styles/Navbar.module.css";
import Image from "next/image";

const Navbar = () => {
    return ( 
        <nav className={`${styles.customnav} navbar navbar-expand-lg`}>
  <div className="container-xl">
    <Link href="/" className="navbar-brand"><Image src="/logo.jpg" alt="pbt-logo" width={200} height={55}/></Link>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <Link className={`${styles.customlink} nav-link`} aria-current="page" href="/">Home</Link>
        </li>
        <li className="nav-item dropdown">
          <a className={`${styles.customlink} nav-link dropdown-toggle`} role="button" data-bs-toggle="dropdown" >
            About
          </a>
          <ul className="dropdown-menu">
            
            <li><Link className={`dropdown-item`} href="/about/about">About Us</Link></li>
            <li><Link className={`dropdown-item`} href="/about/our-mission">Our Mission</Link></li>
            <li><Link className={`dropdown-item`} href="/about/our-people">Our People</Link></li>
            <li><Link className={`dropdown-item`} href="/about/anuradha-story1">Anuradha's Story</Link></li>
            <li><Link className={`dropdown-item`} href="/about/medical-negligence">Why Medical Negligence?</Link></li>
          </ul>
        </li>

        <li className="nav-item dropdown">
          <a className={`${styles.customlink} nav-link dropdown-toggle`} role="button" data-bs-toggle="dropdown" >
            Victims
          </a>
          <ul className="dropdown-menu">
            <li><Link className={`dropdown-item`} href="/victims/victims">Victims</Link></li>
            <li><Link className={`dropdown-item`} href="/victims/medical-opinion">Expert Medical Opinion</Link></li>
            <li><Link className={`dropdown-item`} href="/victims/financial-support">Financial Support</Link></li>
            <li><Link className={`dropdown-item`} href="/victims/medical-negligence-law">Medical Negligence Law</Link></li>
            <li><Link className={`dropdown-item`} href="/victims/judgements">Judgements</Link></li>
            <li><Link className={`dropdown-item`} href="/victims/other-victims">Other Victims</Link></li>
          </ul>
        </li>
        <li className="nav-item">
          <Link className={`${styles.customlink} nav-link`} aria-current="page" href="/membership">Membership</Link>
        </li>
        <li className="nav-item">
          <Link className={`${styles.customlink} nav-link`} aria-current="page" href="/support-us">Support Us</Link>
        </li>
        <li className="nav-item">
          <Link className={`${styles.customlink} nav-link`} aria-current="page" href="/contact">Contact Us</Link>
        </li>
      </ul>
      <Link className={`${styles.btnred} nav-link`} aria-current="page" href="/">Donate Now</Link>
    </div>
  </div>
</nav>
     );
}
 
export default Navbar;